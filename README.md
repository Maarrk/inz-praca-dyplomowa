# inz-praca-dyplomowa

Praca przejściowa inżynierska realizowana w semestrze letnim 2018/2019.

Zakład: [Automatyki i Osprzętu Lotniczego](https://www.meil.pw.edu.pl/zaiol/ZAiOL/)

Kierujący pracą: [dr inż. Przemysław Bibik](https://www.meil.pw.edu.pl/zaiol/ZAiOL/Pracownicy2/Przemyslaw-Bibik)

Tytuł pracy: "Projekt i wykonanie systemów awioniki do bezzałogowego śmigłowca zespolonego"

# TODO:

## Redakcja:

- Wybór fontów
    - szeryfowa Times New Roman
    - bezszeryfowa Lato
- Dostosowanie do zasad redagowania (**wymagane trzy poziomy podrozdziałów**):
    - Odwołania
    - Bibliografia
    - Tytuł tabeli
    - Podpis rysunku
    - Żródło rysunku lub tabeli

## Elementy do opisania:

- Weryfikacja i walidacja