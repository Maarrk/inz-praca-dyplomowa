\section{Wymagania wobec systemu}

Długofalowym celem tworzenia platformy testowej w projekcie ARCHER jest obniżenie progu wej\-ścia do prowadzenia badań na śmigłowcach bezzałogowych.
Jednym ze sposobów na osiągnięcie tego celu jest podział całego statku powietrznego na jednostkę centralną składającą się z~kadłuba wraz z~wir\-ni\-kiem nośnym oraz dodatkowe moduły.
Dzięki zastosowaniu tego rozwiązania, aby sprawdzić w~locie inną konfigurację śmigłowca lub dodatkowy system związany z~wykonywaną misją wystarczy przygotować tylko oryginalne elementy.
Wszystkie najbardziej skomplikowane i~najdroższe podsystemy, w~tym au\-to\-pi\-lot oraz mechanizm sterowania wirnikiem mogą być ponownie wykorzystane w~różnych próbach.

Ponieważ projektowany w~niniejszej pracy system jest częścią jednostki centralnej wyróżnionej na rysunku \ref{fig:jednostkacentralna}, stawia to przed nim unikalne wymagania.
Po pierwsze, oprócz realizacji zadań znanych w momencie projektowania, należy umożliwić dodawanie nowych przyrządów pomiarowych i przeprogramowanie systemu (ang. \emph{futureproofing}).
Po drugie, należy ułatwić wykorzystanie systemu przez inne zespoły.
Powinien być dobrze udokumentowany i możliwie zrozumiały także bez dokumentacji.
Dodatkową zaletą jest wykorzystanie wolnego oprogramowania, tak aby użytkowanie nie wymagało dodatkowych wydatków związanych z licencjami.

\begin{figure}[H]
	\includegraphics[width=\linewidth]{obrazy/jednostkacentralna.png}
	\captionof{figure}{Struktura śmigłowca ARCHER}
	\label{fig:jednostkacentralna}
\end{figure}

\subsection{Mierzone wielkości}

Na etapie formułowania założeń systemu konieczny był wybór parametrów lotu, które należy mierzyć i~zapisywać.
Jednym ze wskaźników użyteczności parametru jest wykorzystanie go w~lotnictwie ogólnym.
Przyrządy, które występują najczęściej w~kokpitach śmigłowców zostały uszeregowane według użyteczności dla pilotów przez \cite{Alppay2015}.
Kolejnym źródłem do oceny przydatności danej wielkości są prace naukowe opisujące projekty bezzałogowych statków powietrznych \cite{Lorenz2011} oraz \cite{Zhu2012}.
W~otoczeniu autora powadzone są także badania związane z~identyfikacją modelu dynamicznego statków powietrznych, w~tym także śmigłowców.
Z~tego powodu w~a\-na\-li\-zie literatury uwzględniono także przykłady badań prowadzonych na ten temat, tj.  \cite{Schafroth2010}, \cite{Taha2011}, \cite{Tang2014}.
Ostatnią grupą źródeł były prace związane ze śmigłowcami zespolonymi \cite{Ferguson2015},  \cite{Moodie2012}, \cite{Yeo2009}.

Na tej podstawie wyciągnięto kilka wniosków.
Po pierwsze, naj\-waż\-niej\-szą informacją o~stanie lotu śmigłowca jest sześć stopni swobody położenia bryły sztywnej w~przestrzeni.
W~różnych formach ta informacja była wykorzystywana we wszystkich uwzględnionych źródłach.
Przyrządy wskazujące wysokość, orientację oraz prędkości są według pilotów naj\-waż\-niej\-sze.
Urządzenia nawigacyjne też są użyteczne w~kokpicie, natomiast nie wymagają tak częstej obserwacji w~lotach załogowych.

W~przypadku bezzałogowych statków powietrznych występuje odwrotna sytuacja, tzn. stabilizacja lotu odbywa się automatycznie, a~głównym zadaniem operatora jest nawigacja.
Jest to odzwierciedlone w~interfejsach graficznych dostępnego oprogramowania do kontroli BSP\footnote{Bezzałogowy Statek Powietrzny}, w~których mapa z~planem misji zajmuje większą część ekranu.
Prędkości kątowe i~przyspieszenia mogą być bezpośrednio mierzone z~dużą częstotliwością, dlatego sa wykorzystywane zarówno w~identyfikacji modelu dynamicznego, jak i~przy tworzeniu oraz ewaluacji układów sterowania.
\nomenclature{BSP}{Bezzałogowy Statek Powietrzny}

Na kolejną grupę składają się wielkości opisujące pracę wirnika nośnego.
Podstawowa z~nich, wy\-mie\-nio\-na w~licznych badaniach, oraz niezbędna w~kokpicie to prędkość obrotowa wirnika.
Dla identyfikacji mo\-de\-lu oraz badań wytrzymałościowych istotny jest także kąt wahania łopat, jednak w~projektowanym śmigłowcu zastosowano głowicę sztywną.
Trudno jest bezpośrednio zmierzyć w~locie naj\-waż\-niej\-szy efekt pracy wirnika nośnego, czyli wytwarzany ciąg.
W~literaturze wykorzystywane są metody pośrednie, polegające na obliczeniu ciągu na podstawie znanych prędkości napływu, prędkości obrotowej wirnika oraz kątów nastawienia łopat.

Ostatnią grupę często powtarzających się parametrów tworzy opis stanu zespołu napędowego.
Przyjmując następujące abstrakcyjne określenia, te same wielkości są wykorzystywane w~kontekście silników turbinowych, tłokowych oraz elektrycznych.
Ponownie, ważna jest prędkość obrotowa.
Ponadto, wykorzystywane są takie wskazania jak: konsumpcja zasobów energii w~jednostce czasu, pozostałe zasoby energii, temperatura elementów zespołu napędowego.
Te wielkości są użyteczne dla planowania lotu, a~także do oceny osiągów danego śmigłowca.

Przykład uszeregowania przyrządów w~zaproponowane kategorie można zaobserwować na ry\-sun\-ku~\ref{fig:kokpit}.
Kolorem niebieskim zaznaczono wyświetlacze przyrządów powiązane z~ruchem całego statku powietrznego.
Na zielono wyróżniono obrotomierz wirnika, a~na pomarańczowo przyrządy informujące o~pracy zespołu napędowego.
Śmigłowiec Robinson~R44 został wybrany jako reprezentatywny na podstawie jego dużej popularności \cite{GAMA2017}.
W~doborze informacji prezentowanych pilotowi i~ich rozmieszczeniu na panelach można zaobserwować zależności opisywane powyżej.

Na tej podstawie zdecydowano że system będzie mierzył i zapisywał następujące parametry lotu:
\begin{itemize}
	\item położenie, orientacja i prędkości statku powietrznego
	\item prędkość obrotowa wirnika nośnego, silników i pomocniczych śmigieł
	\item pośrednio ciąg wirnika nośnego
	\item zużycie prądu
	\item napięcie akumulatorów
	\item temperatura otoczenia i/lub silników
\end{itemize}

\newpage

\begin{figure}[H]
	\includegraphics[width=12cm]{obrazy/kokpit.png}
	\captionof{figure}{Kokpit popularnego śmigłowca Robinson R44.}
	\label{fig:kokpit}
\end{figure}

\subsection{Właściwości statku powietrznego} \label{sec:wlasciwosci}

{\noindent
\label{tab:archer_info}
\captionof{table}{Podstawowe właściwości śmigłowca ARCHER}
\begin{tabular}{ |l|r l| }
	\hline
	Maksymalna masa startowa & $12.5$ & $\text{kg}$ \\
	\hline
	Masa własna & $4.85$ & $\text{kg}$ \\ 
	\hline
	Prędkość postępowa & $250$ & $\frac{\text{km}}{\text{h}}$ \\
	\hline
	Promień wirnika & $888$ & $\text{mm}$ \\
	\hline
	Liczba łopat wirnika & $2$ &  \\
	\hline
	Typ śmigła ogonowego & \multicolumn{2}{c|}{DDVP} \\
	\hline
	Promień śmigła ogonowego & $140$ & $\text{mm}$ \\
	\hline
	Liczba łopat śmigła ogonowego & $2$ &  \\
	\hline
	Długość całkowita & $1870$ & $\text{mm}$ \\ 
	\hline
	Wysokość & $360$ & $\text{mm}$ \\
	\hline
\end{tabular}
}
\vspace{\baselineskip}

Projektowanie systemów elektrycznych rozpoczęto równolegle z~kadłubem.
Z~tego powodu niektóre wielkości trzeba było oszacować przed oblotem prototypu.
Pomocą w~tym procesie było wykorzy\-stanie innego śmigłowca podobnego rozmiaru zbudowanego w~innym projekcie.
Opisywana konstrukcja na bazie modelu zdalnie sterowanego T-REX~700 ma masę startową $9\text{kg}$ oraz sztywną dwułopatową głowicę wirnika.
Dzięki temu jest wystarczająco podobna do ARCHER, żeby móc zamontować na niej te same łopaty i~odbyć próbne loty.
Zamontowano też złożenie śmigła ogonowego typu DDVP\footnote{\emph{Direct Drive Variable Pitch}} składające się z~osobnego silnika i~mechanizmu zmiany skoku, aby sprawdzić jego działanie z~wykorzystanym wirnikiem nośnym.
W~ten sposób zmie\-rzo\-no moc pobieraną w~zawisie dla śmigłowca \mbox{T-REX} $ P_T = 800\text{W} $.
Dla zawisu można pominąć efekty aerodynamiczne kadłuba i~przeliczyć moc na śmigłowiec ARCHER zwiększając wymagany ciąg dla odpowiednio wyższej masy startowej.
\nomenclature{DDVP}{\emph{Direct Drive Variable Pitch}}

Ze wzoru \ref{eq:power_induced} na moc indukowaną dla teorii strumieniowej według \cite{Seddon2011} można zauważyć zależność \ref{eq:power_mass_ratio}, jeśli wszystkie pozostałe wielkości potraktuje się jako stałe.
Następnie podstawiając dane do równania \ref{eq:power_archer} można przybliżyć moc niezbędną dla zawisu ARCHER.
Do napędu wirnika głównego zastosowano silnik PYRO~650, którego moc maksymalna wynosi $ 2\text{kW} $.
Jednak spo\-dzie\-wa\-ny profil misji zakłada duży udział lotu w~zawisie, stąd moc obliczona w~równaniu~\ref{eq:power_archer} będzie podstawą do obliczeń termicznych.
Krótkotrwały wzrost pobieranej mocy, na przykład przy wykonywaniu manewrów nie wpływa znacząco na temperatury elementów, ale może być istotny dla oceny jakości algorytmów sterowania.
Z~tego powodu zakres przyrządów pomiarowych będzie dobierany dla mocy maksymalnej.

\begin{equation}
P_i = T_i v_i = m g \cdot \sqrt{\frac{m g}{2 \rho A_r}}
\label{eq:power_induced}
\end{equation}
\nomenclature{$P_i$}{Moc indukowana}
\nomenclature{$T_i$}{Ciąg w zawisie}
\nomenclature{$v_i$}{Prędkość indukowana}
\nomenclature{$m$}{Masa}
\nomenclature{$g$}{Przyspieszenie grawitacyjne}
\nomenclature{$A_r$}{Powierzchnia dysku wirnika}

\begin{equation}
P_i \sim m^{\frac{3}{2}}
\label{eq:power_mass_ratio}
\end{equation}

\begin{equation}
P_A = P_T \cdot \left(\frac{m_A}{m_T}\right)^\frac{3}{2} = 800\text{W} \cdot \left(\frac{12.5\text{kg}}{9\text{kg}}\right)^\frac{3}{2} = 1309 \text{W}
\label{eq:power_archer}
\end{equation}
\nomenclature{$P_A$}{Moc indukowana śmigłowca ARCHER}
\nomenclature{$P_T$}{Moc indukowana śmigłowca T-REX~700}
\nomenclature{$m_A$}{Masa śmigłowca ARCHER}
\nomenclature{$m_T$}{Masa śmigłowca T-REX~700}

W~powyższych rozważaniach pominięto śmigło ogonowe.
Zakładając, że w~zawisie profil łopaty pracuje w zakresie liniowym, moment oporowy wirnika rośnie proporcjonalnie z ciągiem.
Stąd można dalej wnioskować, że ciąg wytwarzany na śmigle ogonowym także rośnie proporcjonalnie, więc moc indukowana śmigła ogonowego zmienia się podobnie do mocy indukowanej wirnika nośnego.
Innym źródłem błędu jest spadająca sprawność w~zawisie wraz ze wzrostem obciążenia powierzchni dysku wirnika.

\subsection{Walory eksploatacyjne}

Przed rozpoczęciem projektowania układów elektronicznych sformułowano szereg zasad, które mają poprawić jakość projektu oraz ułatwić eksploatację systemu przez innych użytkowników.
Część wynika z~powszechnie stosowanych dobrych praktyk, inne mają ułatwić zapamiętanie niektórych elementów dokumentacji.

Zasady sformułowane przed rozpoczęciem projektowania:

\begin{itemize}
	\item Wykorzystanie fabrycznych kabli podłączanych urządzeń tam gdzie możliwe
	\item Złącza, które da się wpiąć tylko do poprawnego gniazda i~tylko w~dobrej orientacji
	\item Źródło zasilania ma styk żeński
	\item Napięcie zasilające na styku nr 1, czerwony przewód
	\item Napięcie odniesienia (,,masa'') na ostatnim styku, czarny przewód
	\item Spójna kolorystyka sygnałów świetlnych wszystkich urządzeń
	\item Rozmieszczenie elementów elektronicznych tylko po jednej ze stron obwodu drukowanego
\end{itemize}

Główną wadą używania tylko jednej strony płytek jest zwiększanie wymiarów ukończonego ob\-wo\-du.
Jednak w~tym projekcie nie jest to poważny problem, ponieważ największe elementy są mo\-co\-wa\-ne przelotowo, poza tym wymiar płyt jest podyktowany przez możliwości mocowania do struk\-tu\-ry śmigłowca oraz dostępu do złącz tylko przez otwory w~płytach kadłuba.
Takie rozwiązanie ma za to swoje zalety.
Montaż i rozwiązywanie problemów jest dużo łatwiejsze jeśli wszystkie elementy są jednocześnie widoczne, także automatyczny montaż, tzw. \emph{pick'n'place}, jest znacznie tańszy w~takim przypadku.

\newpage